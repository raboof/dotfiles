# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let
  nixpkgs-unstable = import (/home/aengelen/nixpkgs-unstable) { };
in
{
  nix.nixPath = [ "nixpkgs=/home/aengelen/nixpkgs" "nixos-config=/etc/nixos/configuration.nix" "/nix/var/nix/profiles/per-user/root/channels" ];

  nix.extraOptions = 
    #let
    #  hook = pkgs.writeScript "my-post-build-hook" ''
    #    echo 'baz!'
    #  '';
    #in
    ''
      experimental-features = nix-command flakes
      #post-build-hook = $hook
    '';
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      #/home/aengelen/dev/trustix/nixos
    ];
  #nix.settings.substituters = [ "https://cache.dataaturservice.se/spectrum/" ];
  #nix.settings.trusted-public-keys = [
  #  "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
  #  "spectrum-os.org-1:rnnSumz3+Dbs5uewPlwZSTP0k3g/5SRG4hD7Wbr9YuQ="
  #];

  nixpkgs.config.allowUnfree = false;
  nixpkgs.config.checkMetaRecursively = true;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "hplip"
    #"canon-cups-ufr2"
  ];

  fonts.fonts = [
    # symbols, e.g. used by polybar
    pkgs.siji
    # japanese
    pkgs.ipafont
    pkgs.kochi-substitute
    # generic
    pkgs.source-sans-pro
    pkgs.dejavu_fonts
    pkgs.roboto
    pkgs.roboto-mono
    # basically to get archivo, but huge
    pkgs.google-fonts
    pkgs.powerline-fonts
    # testing with notion
    pkgs.jetbrains-mono
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  # To support NTFS-formatted USB drives
  boot.supportedFilesystems = [ "ntfs" ];
  # disable after a boot or two to prevent NVRAM wearout
  #boot.loader.efi.canTouchEfiVariables = true;

  boot.loader.grub.useOSProber = true;

  # for generating rpi images
  #boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
  #boot.binfmt.registrations.aarch64 =
  #  {
  #  interpreter = "${pkgs.qemu}/bin/qemu-aarch64";
  #  magicOrExtension = ''\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00'';
  #  mask = ''\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\x00\xff\xfe\xff\xff\xff'';
  #  };

  boot.kernelParams = [
    "boot.shell_on_fail"
  ];

  # disable bluetooth for security
  boot.blacklistedKernelModules = [ "btusb" ];

  #services.trustix = {
  #  enable = true;
  #  signers.signer-raboof = {
  #    type = "ed25519";
  #    ed25519.private-key-path = "/etc/nixos/secrets/log-priv";
  #  };
  #  publishers = [
  #    {
  #      signer = "signer-raboof";
  #      protocol = "nix";
  #      meta = {
  #        upstream = "https://cache.nixos.org";
  #      };
  #      publicKey = {
  #        type = "ed25519";
  #        key = builtins.readFile ./secrets/log-priv;
  #      };
  #    }
  #  ];
  #};
  #services.trustix-nix-build-hook = {
  #  enable = true;
  #  publisher = "a1c31b9b0a9163188fe56788ffbd7c9d188403b8f27422bf0320b4b3414d026f";
  #};

  boot.extraModulePackages = [ config.boot.kernelPackages.v4l2loopback ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "rigter"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;
  # this doesn't seem to disable IPv6 entirely? but enough to work around hack42 problems?
  networking.enableIPv6 = false;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # https://bugs.freebsd.org/bugzilla/show_bug.cgi?id=137870
  i18n.defaultLocale = "en_DK.UTF-8";

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  containers.mqtt = {
    autoStart = true;
    privateNetwork = false;
    config = { config, pkgs, ...}: {
      services.mosquitto = {
        enable = true;
        listeners = [ {
          acl = [ "pattern readwrite #" ];
          omitPasswordAuth = true;
          settings.allow_anonymous = true;
        } ];
      };
      networking.firewall = {
        enable = true;
        allowedTCPPorts = [ 1883 ];
      };
      system.stateVersion = "23.05";
    };
  };
  
  #virtualisation.docker = {
  #  enable = true;
  #  daemon.settings = {
  #    hosts = [ "fd://" "tcp://0.0.0.0:2375" ];
  #  };
  #};
  #virtualisation.virtualbox.host.enable = true;
  virtualisation.lxc = {
    enable = true;
    defaultConfig = ''
      lxc.net.0.type = veth
      lxc.net.0.mode = bridge
    '';
  };
  virtualisation.lxd.enable = true;

  # testing out an ISO
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;
  boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

  #hardware.opengl.package = nixpkgs-mesa.mesa.drivers;
  hardware.sane.enable = true;
  hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];
 
  environment.etc."xdg/mimeapps.list" = {
    text = ''
      [Default Applications]
      text/html=firefox.desktop
      x-scheme-handler/http=firefox.desktop
      x-scheme-handler/https=firefox.desktop
      x-scheme-handler/mailto=thunderbird.desktop
    '';
  };
  environment.variables.EDITOR = "vim";

  environment.systemPackages = with pkgs; [
    networkmanager-vpnc
    pavucontrol
    unclutter-xfixes
    nethogs
    screen
    unzip
    #manpages
    zip
    vim_configurable
    polybar
    # also introduces default /etc/ssl/openssl.cnf
    openssl
    #obs-studio
    evince
    bc
    redshift
    #i3lock
    imagemagick
    pwgen
    inotify-tools
    nix-index
    pciutils
    acpi
    #glxinfo
    powertop
    alsa-utils
    wget
    file
    killall
    usbutils
    ccid
    pcsclite
    binutils-unwrapped
    bind # for dig
    whois
    gdb
    pastebinit
    gnupg
    gitFull
    gitAndTools.hub
    github-cli
    arandr
    firefox
    kitty
    nixpkgs-review
    easyrsa
    wireshark

    volare
    dmenu
    swaylock
    grim slurp # screenshots: 'grim -g "$(slurp)" out.png'
    mako # notifications
    meld
    # termite is nice, with support for url hinting,
    # but kitty can url-hint as well as support
    # ':alsamixer' from notion
    #(nixpkgs-unstable.termite.overrideAttrs (oldAttrs: {
    #  patches = [
    #    (fetchpatch {
    #      name = "url-hints-with-alt-f";
    #      url = "https://github.com/raboof/termite/commit/5eed55e6a09e8c54f0e8f5f671f55ce09019c9f6.patch";
    #      sha256 = "1ws4a1wradark3jbdvgrr0pmxd29bz3zsw93wsl3560x86xclh9c";
    #    })
    #  ];
    #}))
    #(nixpkgs-vscode.vscode-with-extensions.override {
    #  vscodeExtensions = with nixpkgs-vscode.vscode-extensions; [
    #    scala-lang.scala
    #    scalameta.metals
    #  ];
    #})
    #(nixpkgs-vscode.vscode-with-extensions.override {
    #(nixpkgs-vscode.vscode-with-extensions.override {
    #  vscode = nixpkgs-vscode.vscodium;
    #  vscodeExtensions = with nixpkgs-vscode.vscode-extensions; [
    #    scala-lang.scala
    #    scalameta.metals
    #  ];
    #})
    #(vscode-with-extensions.override {
    #  vscode = vscodium;
    #  vscodeExtensions = with vscode-extensions; [
    #  ];
    #})
  ];

  # to allow 'save as' in Arduino...
  #programs.dconf.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  #programs.gnupg.agent = { enable = true; };
  #programs.ssh.startAgent = true;
  programs.wireshark.enable = true;

  #programs.zsh.enable = true;
  #programs.zsh.ohMyZsh = {
  #  enable = false;
  #  plugins = [ ];
  #  theme = "robbyrussell";
  #};
  #programs.fish.enable = true;

  # https://github.com/bennofs/nix-index#usage-as-a-command-not-found-replacement
  # https://github.com/NixOS/nixpkgs/issues/39789
  #programs.command-not-found.enable = false;
  #programs.bash.interactiveShellInit = ''
  #  source ${pkgs.nix-index}/etc/profile.d/command-not-found.sh
  #'';
  #programs.zsh.interactiveShellInit = ''
  #  source ${pkgs.nix-index}/etc/profile.d/command-not-found.sh
  #'';

  #services.tomcat = {
  #  enable = true;
  #  jdk = pkgs.jdk8;
  #};
  #services.httpd.enable = true;
  #services.httpd.virtualHosts = {
  #  "localhost" = {
  #    documentRoot = "/tmp/archive-header-test";
  #    forceSSL = false;
  #  };
  #};

  #services.davfs2.enable = true;
  #services.zookeeper.enable = true;
  # let applications do DNS themselves so we can intercept it
  # with opensnitch
  services.nscd.enable = false;
  system.nssModules = lib.mkForce [];

  # List services that you want to enable:
  # services.pcscd.enable = true;
  services.udev.packages = [
    pkgs.yubikey-personalization
    pkgs.android-udev-rules
  ];

  #services.cron = {
  #  enable = true;
  #  systemCronJobs = [
  #    "15 * * * * aengelen ${nixpkgs-mastodon-bot.mastodon-bot}/bin/mastodon-bot /home/aengelen/dev/mastodon-bot-nixos/config.hack42.edn >> /tmp/cron.log"
  #    "30 * * * * aengelen ${nixpkgs-mastodon-bot.mastodon-bot}/bin/mastodon-bot /home/aengelen/dev/mastodon-bot-nixos/config-mch.edn >> /tmp/cron.log"
  #    "45 * * * * aengelen ${nixpkgs-mastodon-bot.mastodon-bot}/bin/mastodon-bot /home/aengelen/dev/mastodon-bot-nixos/config-rb.edn >> /tmp/cron.log"
  #  ];
  #};

  #services.keybase.enable = true;
  #services.kbfs.enable = true;

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # populate /etc/hosts
  #networking.extraHosts = ''
  #  127.0.0.1 example.com
  #  127.0.0.1 akka.example.org
  #  127.0.0.1 here.mtls.proxy.com
  #'';

  # Open ports in the firewall.
  #networking.firewall.allowedTCPPorts = [ 2375 ];
  networking.firewall.allowedUDPPorts = [ 
    # wireguard
    51820
    # MQTT
    1883
  ];
  # disabled for now to avoid IP address clashes
  #networking.wireguard.interfaces = {
  #  wg0 = {
  #    ips = [ "192.168.43.6/24" ];
  #    listenPort = 51820;
  #    privateKeyFile = "/home/aengelen/wireguard-keys/private";
  #    peers = [
  #      {
  #        publicKey = "lqhr3JhYX+6X2VK8EjiUPFlq+XYIxBSvHaiH4XiDZjg=";
  #        allowedIPs = [ "192.168.43.0/24" "192.168.142.0/24" "192.168.0.0/16" ];
  #        endpoint = "185.205.52.134:51820";
  #        persistentKeepalive = 25;
  #      }
  #    ];
  #  };
  #};

  services.earlyoom = {
    enable = true;
    freeMemThreshold = 4;
  };
  
  #services.opensnitch.enable = true;
  #services.opensnitchd.enable = true;

  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  #networking.resolvconf.dnsExtensionMechanism = false;
  #programs.captive-browser.enable = true;
  #programs.captive-browser.interface = "wlp59s0";
  services.searx = {
    enable = false;

    settings = {
      use_default_settings.engines.remove = "google";
      engines = [
        {
          name = "duckduckgo";
        }
      ];
      server = {
        port = 3030;
        bind_address = "::1";
        secret_key = "Jox9Ooc5thaX9uM9";
      };
    };
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;
  services.printing.drivers = [
    pkgs.hplip
    # hack42 printer
    #pkgs.canon-cups-ufr2
  ];
  programs.system-config-printer.enable = true;

  # Enable sound.
  #sound.enable = true;
  # https://nixos.wiki/wiki/Bluetooth
  hardware.pulseaudio = {
    #enable = true;
    # For bluetooth support:
    #package = pkgs.pulseaudioFull;
  };
  hardware.bluetooth.enable = false;
  services.blueman.enable = false;

  # audio:
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
    jack.enable = true;
  };

  security.pam.services.swaylock = {};

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.videoDrivers = [ "intel" ];
  services.xserver.desktopManager.xfce.enable = true;
  #services.xserver.displayManager.gdm.wayland.enable = true;
  services.xserver = {
    windowManager.notion.enable = true;
    windowManager.fluxbox.enable = true;
    # or 'xfce+notion' / 'xterm+notion'?
    displayManager.defaultSession = "none+notion";
    displayManager.autoLogin.enable = false;
    displayManager.autoLogin.user = "aengelen";
  };
  # https://github.com/NixOS/nixpkgs/issues/123687
  #services.redshift = {
  #  enable = true;
  #  executable = "/bin/redshift-gtk";
  #  # to avoid redshift and xrandr fighting over the brightness:
  #  extraOptions = [ "-m vidmode" ];
  #};
  location.latitude = 52.26;
  location.longitude = 6.16;
  #programs.sway.enable = true;

  # hrm, unfree/
  # https://nixos.wiki/wiki/Nvidia
  #services.xserver.videoDrivers = [ "modesetting" "nvidia" ];
  #hardware.nvidia.prime.sync.enable = true;
  #hardware.nvidia.prime.nvidiaBusId = "PCI:1:0:0";
  #hardware.nvidia.prime.intelBusId = "PCI:0:2:0";

  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable the KDE desktopManager.plasma5.enable = true;
  services.fwupd.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    #defaultUserShell = pkgs.nushell;
    #defaultUserShell = pkgs.bash;
    users.aengelen = {
      isNormalUser = true;
      extraGroups = [
        "libvirtd"
        "vboxusers"
        "wheel"
        "dialout"
        "networkmanager"
        "docker"
        "wireshark"
        # Connect to Android
        "plugdev"
        "lxd"
        "video"
        "davfs2"
        "tomcat"
      ];
      subUidRanges = [ { startUid = 100000; count = 65536; } ];
      subGidRanges = [ { startGid = 100000; count = 65536; } ];
    };
    #users.guest= {
    #  isNormalUser = true;
    #  extraGroups = [
    #    "dialout"
    #    "networkmanager"
    #  ];
    #};

  };
  system.copySystemConfiguration = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.03"; # Did you read the comment?
}
