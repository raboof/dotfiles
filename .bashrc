source ~/.secretsrc
export DEFAULT_USER=aengelen
source ~/dev/agnoster-bash/agnoster.bash
export PATH=~/bin:$PATH

# don't overwrite history
shopt -s histappend
# update history immediately
PROMPT_COMMAND="$PROMPT_COMMAND; history -a; history -n"

alias ns=~/dev/nix-shell-git/nix-shell-git

# https://twitter.com/thingskatedid/status/1316074032379248640
alias icat="kitty icat --align=left"
# actually icat seems to support isvg just fine
#alias isvg="rsvg-convert | icat"

# GoLang
#export GOPATH=/home/aengelen/go
#export PATH=$GOPATH/bin:$PATH
